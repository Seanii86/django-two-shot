from receipts.models import Receipt, ExpenseCategory, Account
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView
from django.shortcuts import redirect


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = 'receipts/list.html'

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreate(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = 'receipts/create_receipt.html'
    fields = ["vendor", "total", "tax", "date", "category", "account"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.purchaser = self.request.user
        item.save()
        return redirect('home')


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = 'receipts/expenses.html'

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class AccountsView(LoginRequiredMixin, ListView):
    model = Account
    template_name = 'receipts/accounts_list.html'

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = 'receipts/create_category.html'
    fields = ["name"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect('list_categories')


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = 'receipts/create_account.html'
    fields = ["name", "number"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect('list_account')
