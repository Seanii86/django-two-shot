from django.urls import path
from receipts.views import (
    ReceiptListView,
    ReceiptCreate,
    ExpenseCategoryListView,
    AccountsView,
    ExpenseCategoryCreateView,
    AccountCreateView
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreate.as_view(), name="create_receipt"),
    path("categories/", ExpenseCategoryListView.as_view(), name="list_categories"), # noqa
    path("accounts/", AccountsView.as_view(), name="list_account"),
    path("categories/create/", ExpenseCategoryCreateView.as_view(), name="create_category"),  # noqa
    path("accounts/create/", AccountCreateView.as_view(), name="create_account"), # noqa
]
